<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\User;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Product::orderBy('created_at')
                ->select('products.*','users.id AS user_id','users.name')
                ->join('users', 'users.id', '=', 'products.seller_id')
                ->paginate(5);
        return $items;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Product::create([
            'seller_id' =>  auth('api')->user()->id,
            'product_code' =>  $request['product_code'],
            'product_name' =>  $request['product_name'],
            'photo' =>  '',
            'price' =>  $request['price'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = auth('api')->user()->id;
        $item = Product::findOrFail($id);

        $currentPhoto = $item->photo;

        $this->validate($request,[
            'product_name' => 'required|string|max:191',
            'product_code' => 'required|string|max:191|unique:products,product_code,'.$item->id,
            'price' => 'required|integer|digits_between:1,8'
        ]);

        if($request->photo != $currentPhoto){
            $name = $user_id.'_'.$request->product_code.'_'.time().'.'.explode('/',explode(':',substr($request->photo,0,strpos($request->photo, ';')))[1])[1];

            \Image::make($request->photo)->save(public_path('img/items/').$name);

            $request->merge(['photo' => $name]);

            $userPhoto = public_path('img/items/').$currentPhoto;
            if(file_exists($userPhoto)){
                unlink($userPhoto);
            }
        }

        return ['message',$request->all()];
        die;
        $item->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Product::findOrFail($id);
        $item->delete();
    }

    public function search() {
        if($search = \Request::get('q')){
            $items = Product::where(function($query) use ($search){
                $query->where('product_code','LIKE',"%$search%")
                      ->orWhere('product_code','LIKE',"%$search%");
            })->paginate(5);
        } else {
            $items = Product::orderBy('id')->paginate(5);
        }
        return $items;
    }
}
