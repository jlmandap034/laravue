<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\User;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')
                    ->select('posts.*','users.id AS user_id','users.name')
                    ->join('users', 'users.id', '=', 'posts.author_id')
                    ->paginate(5);
        return $posts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth('api')->user();
        return Post::create([
            'author_id' => $user->id,
            'title' =>  $request['title'],
            'body' =>  $request['body'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function posts()
    {
        $id = auth('api')->user()->id;
        return Post::orderBy('created_at','desc')
                    ->select('posts.*','users.id AS user_id','users.name','users.email'
                        ,'users.password','users.bio','users.photo','users.type')
                    ->join('users', 'users.id', '=', 'posts.author_id')
                    ->where('author_id','=',$id)
                    ->paginate(5);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$this->authorize('isAdminORAuthor');
        $post = Post::findOrFail($id);
        $post->delete();
    }

    public function search() {
        if($search = \Request::get('q')){
            $users = Post::orderBy('created_at','desc')
                    ->select('posts.*','users.id AS user_id','users.name')
                    ->join('users', 'users.id', '=', 'posts.author_id')
                    ->where('title','LIKE',"%$search%")
                    ->orWhere('body','LIKE',"%$search%")
                    ->orWhere('name','LIKE',"%$search%")
                    ->paginate(5);
        } else {
            $users = Post::orderBy('created_at','desc')
                    ->select('posts.*','users.id AS user_id','users.name')
                    ->join('users', 'users.id', '=', 'posts.author_id')
                    ->paginate(5);
        }
        return $users;
    }
}
