<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAuthUser',function($user,$post){
            return $user->id === $post->author_id;
        });

        Gate::define('isAdmin',function($user){
            return $user->type === 'admin';
        });

        Gate::define('isUser',function($user){
            return $user->type === 'user';
        });

        Gate::define('isAuthor',function($user){
            return $user->type === 'author';
        });

        Gate::define('isSeller',function($user){
            return $user->type === 'seller';
        });

        Gate::define('isAdminORSeller',function($user){
            if($user->type === 'admin' || $user->type === 'seller') {
                return true;
            }
        });

        Gate::define('isAdminORAuthor',function($user){
            if($user->type === 'admin' || $user->type === 'author') {
                return true;
            }
        });

        Gate::define('isAuthorORUser',function($user){
            if($user->type === 'author' || $user->type === 'user') {
                return true;
            }
        });

        Passport::routes();
    }
}
