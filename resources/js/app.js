
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment' // --> DATE FORMAT
import { Form, HasError, AlertError } from 'vform' // --> ERROR MESSAGES IN FORMS

import gate from './gate'; // --> VUE AUTHORIZATION
Vue.prototype.$gate = new gate(window.user);

import Swal from 'sweetalert2' // --> ALERT
window.Swal = Swal;

window.Form = Form; // --> FORMS
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component('pagination', require('laravel-vue-pagination')) // --> PAGINATION

import VueRouter from 'vue-router' // --> ROUTER FOR PAGES
Vue.use(VueRouter)

let routes = [
	{ path: '/dashboard',component: require('./components/Dashboard.vue').default },
	{ path: '/profile',component: require('./components/Profile.vue').default },
	{ path: '/users',component: require('./components/Users.vue').default },
	{ path: '/order',component: require('./components/Order.vue').default },
	{ path: '/products',component: require('./components/Products.vue').default },
	{ path: '*',component: require('./components/NotFound.vue').default }
]

const router = new VueRouter({
	mode: 'history',
	routes
})

import VueProgressBar from 'vue-progressbar' // --> PROGRESS BAR

Vue.use(VueProgressBar, {
	color: 'rgb(143,255,199)',
	faliedColor: 'red',
	height: '3px'
})

// --> UPPER CASE FIRST LETTER
Vue.filter('upText', function(text){
	return text.charAt(0).toUpperCase() + text.slice(1);
});

// --> UPPER CASE WORD
Vue.filter('capText', function(text){
	return text.toUpperCase();
});

// --> MOMENT
Vue.filter('myData', function(created){
	return moment(created).format('MMMM Do YYYY');
});

Vue.filter('myDateTime', function(created){
	return moment(created).format('MMMM Do YYYY, hh:mm A');
});

// --> FIRE FUNCTIONS
window.Fire = new Vue();
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// --> PASSPORT
Vue.component(
	'passport-clients',
	require('./components/passport/Clients.vue').default
);

Vue.component(
	'passport-authorized-clients',
	require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
	'passport-personal-access-tokens',
	require('./components/passport/PersonalAccessTokens.vue').default
);

// --> <not-found></not-found>
Vue.component(
	'not-found',
	require('./components/NotFound.vue').default
);

const app = new Vue({
    el: '#app',
    router,
    data: {
    	search: ''
    },
    methods: {
    	// --> PRINT (INVOICE)
    	printme() {
    		window.print();
    	}
    }
});
