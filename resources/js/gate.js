// AUTHORIZATIONS
export default class gate {
	constructor(user) {
		this.user = user;
	}

	isAuthUserNoAdmin($author_id) {
		return this.user.id === $author_id;
	}

	isAuthUser($author_id) {
		if(this.user.id === $author_id || this.user.type === 'admin') {
			return true;
		}
	}

	isAdmin() {
		return this.user.type === 'admin';
	}

	isUser() {
		return this.user.type === 'user';
	}

	isAuthor() {
		return this.user.type === 'author';
	}

	isSeller() {
		return this.user.type === 'seller';
	}

	isAdminORSeller() {
		if(this.user.type === 'admin' || this.user.type === 'seller') {
			return true;
		}
	}

	isAdminORAuthor() {
		if(this.user.type === 'admin' || this.user.type === 'author') {
			return true;
		}
	}

	isAuthorORUser() {
		if(this.user.type === 'user' || this.user.type === 'author') {
			return true;
		}
	}
}